# Zoo Management System
This is a Java-based Zoo Management System that allows you to manage animals in a zoo. The system provides classes to represent different animals, a Zoo class to manage them, and an abstract Animal class as the base class.
## Animal Class
The Animal class is an abstract class that serves as the base class for all animals in the zoo. It contains attributes such as name and age, along with getter and setter methods. The class also includes common methods like feed() and sleep(), which are overridden by the subclasses.
## Giraffe Class
The Giraffe class extends the Animal class and represents a giraffe in the zoo. It introduces additional attributes specific to giraffes, such as neckLength. The class overrides the feed() method to display a custom message for giraffes and includes methods like stretchNeck() and run().

## Lion Class
The Lion class is another subclass of Animal and represents a lion in the zoo. It includes a specific attribute numberOfCubs to track the number of lion cubs. The class overrides the feed() method with a lion-specific behavior and provides methods like roar() and groom().

## Zoo Class
The Zoo class manages a list of animals in the zoo. It provides methods to add and remove animals from the zoo, as well as to feed all the animals in the zoo. The class uses an ArrayList to store the animals.

![ClassDiagram](ZooClassDiagram.jpg)
