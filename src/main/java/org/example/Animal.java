package org.example;

abstract class Animal {
    private String name;
    private int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void feed();


    public void sleep() {
        System.out.println(name + " is sleeping.");
    }

    public String toString(){
        return String.format("Animal[name=%s, age=%d]",name,age);
    }

}
