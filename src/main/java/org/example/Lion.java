package org.example;

class Lion extends Animal {
    private int numberOfCubs;

    public Lion(String name, int age, int numberOfCubs) {
        super(name, age);
        this.numberOfCubs = numberOfCubs;
    }

    public int getNumberOfCubs() {
        return numberOfCubs;
    }

    public void setNumberOfCubs(int numberOfCubs) {
        this.numberOfCubs = numberOfCubs;
    }

    @Override
    public void feed() {
        System.out.println(getName() + " is hunting for prey.");
    }

    public void roar() {
        System.out.println(getName() + " is roaring loudly.");
    }

    public void groom() {
        System.out.println(getName() + " is grooming its fur.");
    }

    public String toString(){
        return String.format("Lion [%s, numberOfCubs=%d ]",super.toString(),numberOfCubs);
    }
}
