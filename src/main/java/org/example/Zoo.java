package org.example;

import java.util.ArrayList;
import java.util.List;

class Zoo {
    private List<Animal> animals;

    public Zoo() {
        animals = new ArrayList<>();
    }

    public void addAnimal(Animal animal) {
        animals.add(animal);
    }

    public void removeAnimal(Animal animal) {
        animals.remove(animal);
    }

    public void feedAllAnimals() {
        for (Animal animal : animals) {
            animal.feed();
        }
    }

}
