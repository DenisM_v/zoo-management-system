package org.example;

class Giraffe extends Animal {
    private int neckLength;

    public Giraffe(String name, int age, int neckLength) {
        super(name, age);
        this.neckLength = neckLength;
    }

    public int getNeckLength() {
        return neckLength;
    }

    public void setNeckLength(int neckLength) {
        this.neckLength = neckLength;
    }

    @Override
    public void feed() {
        System.out.println(getName() + " is grazing on leaves.");
    }

    public void stretchNeck() {
        System.out.println(getName() + " is stretching its long neck.");
    }

    public void run() {
        System.out.println(getName() + " is running gracefully.");
    }
    public String toString(){
        return String.format("Girrafe [%s, numberOfCubs=%d ]",super.toString(),neckLength);
    }
}
